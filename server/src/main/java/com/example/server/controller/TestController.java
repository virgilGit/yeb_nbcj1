package com.example.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.server.advice.validate.MyRequired;
import com.example.server.advice.validate.MyValidate;
import com.example.server.advice.verify.MyVerify;
import com.example.server.pojo.Admin;
import com.example.server.pojo.Customer;
import com.example.server.utils.RSAUtils;
import com.example.server.vo.RespBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "测试控制器")
public class TestController {

    @ApiOperation("测试1")
    @GetMapping("/test1")
    public String test1() {

        return "success!";
    }

    @ApiOperation("测试2")
    @GetMapping("/test2")
    public RespBean test2() {

        int i = 1/0;
        return RespBean.success();
    }

    /**
     * @RequestParam 从？后面获取值，请求参数
     *  HttpServletRequest#getParameter("adminName")
     *  从form表单中获取值
     *  * 如果参数前不加任何注解，默认采用@RequestParam，默认使用的key值和变量名一致
     * @param name
     * @param age
     * @return
     */
    @GetMapping("/test3")
    public RespBean test3(@RequestParam("adminName") String name, @RequestParam("adminAge") Integer age) {

        return RespBean.success("name:"+name + " age:" + age);
    }

    @GetMapping("/test4")
    public RespBean test4(String name, Integer age) {

        return RespBean.success("name:"+name + " age:" + age);
    }

    @GetMapping("/test5")
    public RespBean test5(Admin admin) {

        return RespBean.success(admin);
    }

    @PostMapping("/test6")
    public RespBean test6(@RequestBody Admin admin) {

        /**
         * 客户端 -> json -> 服务器 -> @RequestBody -> ObjectMapper -> java 对象
         *
         * RespBean (java 对象) -> @ResponseBody -> ObjectMapper -> json -> 客户端
         *
         * @RestController = @ResponseBody + @Controller
         */
        return RespBean.success(admin);
    }

    @PostMapping("/test7")
    public RespBean test7(@RequestBody JSONObject data) {

        System.out.println(data);

        return RespBean.success(data);
    }

    /**
     * /test8/1
     * /test8/2
     * @param cid
     * @return
     */
    @DeleteMapping("/test8/{cid}")
    public RespBean test8(@PathVariable Integer cid) {

        return RespBean.success(cid);
    }

    @GetMapping("/test9")
    public RespBean test9(Integer[] ids) {

        return RespBean.success(ids);
    }

    /**
     * 设计的接口，需要验证签名是否匹配
     * 防止非对接的服务器，访问到我们的服务器中
     */
    @PostMapping("/test/verify")
    @MyVerify
    public RespBean testVerify(@RequestBody Customer customer, @RequestHeader String signature) {

        return RespBean.success(customer);
    }

    @GetMapping("/test/validate")
    @MyValidate
    public RespBean testValidate(@MyRequired String id, @MyRequired String name) {

        /**
         * 测试接口的时候，id和name是必填的值
         * 如果值为空或者空字符串 返回错误信息RespBean
         *
         * 思考题 制作数据范围校验，如以下例子，要求age的大小必须在1~100之间，如果不再范围之内就报错
         * @MyBetween(max= 100, min=1) Integer age
         */

        return null;
    }
}
