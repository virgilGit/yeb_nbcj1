package com.example.server.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小红
 * @since 2022-11-12
 */
@RestController
@RequestMapping("/sys-yeb-log")
public class SysYebLogController {

}
