package com.example.server.controller;


import com.example.server.advice.YebLog;
import com.example.server.pojo.Employee;
import com.example.server.service.IEmployeeService;
import com.example.server.vo.PageBean;
import com.example.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {


    @Autowired
    private IEmployeeService employeeService;

    @GetMapping("/list/page")
    @YebLog(name = "手动分页查询")
    public RespBean listAllPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                @RequestParam(defaultValue = "5") Integer size) {

        /**
         * 1. 收集客户端的请求数据
         * 2. 将数据封装下，传递给service，由service执行业务逻辑
         * 3. service返回执行的结果 （增删改查）
         * 4. 将执行的结果封装，发送给客户端
         *
         * controller中不要写业务逻辑，service才写业务逻辑
         */
        PageBean<Employee> pageResult = employeeService.getAllPage(currentPage, size);

        return RespBean.success(pageResult);
    }

    @GetMapping("/list/page2")
    @YebLog(name = "mybatisplus分页查询")
    public RespBean listAllPage2(@RequestParam(defaultValue = "1") Integer currentPage,
                                 @RequestParam(defaultValue = "5") Integer size) {

        PageBean<Employee> pageResult = employeeService.getAllPage2(currentPage, size);

        return RespBean.success(pageResult);
    }
}
