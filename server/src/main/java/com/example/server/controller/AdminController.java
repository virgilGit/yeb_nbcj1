package com.example.server.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.server.pojo.Admin;
import com.example.server.service.IAdminService;
import com.example.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    @GetMapping("/")
    public RespBean list() {

        List<Admin> list = adminService.list();

        return RespBean.success(list);
    }

    @PostMapping("/")
    public RespBean add(@RequestBody Admin admin) {

        boolean save = adminService.save(admin);

        return save ? RespBean.success() : RespBean.error(301, "新增失败！");
    }

    @PutMapping("/")
    public RespBean update(@RequestBody Admin admin) {

        boolean update = adminService.updateById(admin);

        return update ? RespBean.success() : RespBean.error(302, "修改失败！");
    }

    @DeleteMapping("/{id}")
    public RespBean remove(@PathVariable Integer id) {

        boolean result = adminService.removeById(id);

        return result ? RespBean.success() : RespBean.error(303, "删除失败！");
    }

    @GetMapping("/list")
    public RespBean listByConditions(Admin admin) {

        List<Admin> result = adminService.getByConditions(admin);

        return RespBean.success(result);
    }

    @GetMapping("/count")
    public RespBean listCount() {

        Integer count = adminService.getAllCount();

        return RespBean.success(count);
    }

    @GetMapping("/count2")
    public RespBean listCount2() {

        int count = adminService.count();

        return RespBean.success(count);
    }

    @GetMapping("/count3")
    public RespBean listCount3() {

        /**
         * list[
         * map{
         *     id: 1,
         *     name: xxxx
         * },
         * map {
         *     id: 2,
         *     name: yyyy
         * }
         *
         * ]
         * select count(1) adminCount from t_admin
         */

        List<Map<String, Object>> mapList = adminService.listMaps(new QueryWrapper<Admin>().select("count(1) adminCount"));

        Object count = mapList.get(0).get("adminCount");

        return RespBean.success(count);
    }

    @PostMapping("/save/batch")
    public RespBean saveBatch(@RequestBody List<Admin> admins) {

        boolean result = adminService.saveBatchAdmin(admins);

        return result ? RespBean.success() : RespBean.error(304, "批量新增失败！");
    }
}
