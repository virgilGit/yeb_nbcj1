package com.example.server.advice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD) // 当前的注解只能够修饰METHOD 方法
@Retention(RetentionPolicy.RUNTIME) // 当前注解的生命周期，RUNTIME 运行时依旧有效
public @interface YebLog {
    String name() default "未知的方法";
}
