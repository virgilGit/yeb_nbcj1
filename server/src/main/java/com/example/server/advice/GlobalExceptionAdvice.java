package com.example.server.advice;

import com.example.server.vo.RespBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class GlobalExceptionAdvice {

    private final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionAdvice.class);

    @ExceptionHandler(ArithmeticException.class)
    public RespBean arithmeticExceptionHandler(ArithmeticException exception) {

        // 打印日志
        LOGGER.error("算术异常：" + exception.getMessage() , exception);
        // 将异常翻译翻译，发送给客户端

        return RespBean.error(101, "算术异常：" + exception.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public RespBean runtimeExceptionHandler(RuntimeException e) {

        LOGGER.error("运行时异常：" + e.getMessage());

        return RespBean.error(500, e.getMessage());
    }
}
