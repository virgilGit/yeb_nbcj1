package com.example.server.advice;

import com.example.server.mapper.SysYebLogMapper;
import com.example.server.pojo.SysYebLog;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Component // 和@Service @Controller是一个道理，注册当前的对象到spring容器中
@Aspect // 切面：切入点（哪些方法会被选中） + 增强（编写新的功能，整合到被选中的方法中[执行之前、之后、报错的时候]）
public class YebLogAdvice {

    @Autowired
    private SysYebLogMapper sysYebLogMapper;

    /**
     * 制定一个选择方法的规则
     * 被@YebLog注解修饰的方法，都是选中的方法
     * 对于这些选中的方法，进行增强，需要整合新的功能
     */
    @Pointcut("@annotation(com.example.server.advice.YebLog)")
    public void yebLog() {

    }

    /**
     * 进行增强的时候，有一个问题，也就是增强的代码具体注册到哪儿？
     * 1 方法执行之前
     * 2 方法执行之后
     * 3 方法报错的时候
     * ... => 增强的类型
     * 在制作增强功能的时候，需要首先制定好增强的类型
     *
     *
     */
    @Around("yebLog()") // 设计一个around类型的增强，作用到yebLog()也就是被@YebLog注解修饰的方法上
    public Object yebLogAdvice(ProceedingJoinPoint proceedingJoinPoint) {

        Object result = null;
        /**
         * proceedingJoinPoint springaop的一个对象，用来描述被选中的方法本身的；
         * 方法本身：方法也是一个对象，记录方法名称、参数、注解
         * 甚至我们可以同故宫这个对象手动控制被选中的方法什么时候执行
         *
         * 功能：
         * 1. 调用接口的时候，每调用一次，就会在数据库中新增一条访问记录
         * 2. 准备数据库，生成pojo mapper service controller
         * 3. 在增强中凑齐新增的记录的信息
         * 4. 正常执行调用的接口
         * 5. 将心中的记录插入到数据库中
         */
        SysYebLog sysYebLog = new SysYebLog();
        sysYebLog.setStartDate(LocalDateTime.now());


        MethodSignature signature =
                (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();

        // 被选择的方法的名称
        sysYebLog.setMethodName(method.getName());

        // 被选择的方法的注解中的name值获取到
        // @YebLog注解类，里面是有参数的name
        // 设置name的值：1. default "xxxx" 2. 在使用注解的时候指定@YebLog(name=xxx)
        // 获取name的值：使用Method对象getAnnotation获取注解对象
        // 调用注解对象的name()
        YebLog yebLogAnno = method.getAnnotation(YebLog.class);
        sysYebLog.setMethodInfo(yebLogAnno.name());

        try {
            // TODO before 前置增强
            result = proceedingJoinPoint.proceed();
            // TODO afterReturning 后置增强
            sysYebLog.setStatus(1);
            sysYebLog.setMsg("方法执行成功！");
        } catch (Throwable throwable) {
            // TODO afterThrowing 异常增强
            throwable.printStackTrace();
            sysYebLog.setStatus(0);
            sysYebLog.setMsg(throwable.getMessage());
        } finally {
            // TODO after 最终增强
            sysYebLog.setEndDate(LocalDateTime.now());
            sysYebLogMapper.insert(sysYebLog);
        }

        return result;
    }
}
