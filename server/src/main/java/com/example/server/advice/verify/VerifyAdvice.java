package com.example.server.advice.verify;

import com.example.server.utils.RSAUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect // 切面：切点 + 增强
public class VerifyAdvice {

    @Pointcut("@annotation(com.example.server.advice.verify.MyVerify)")
    public void verify() {

    }

    @Before("verify()")
    public void verifyBefore(JoinPoint joinPoint) {

        // 数据原文 meta reflect
        Object[] args = joinPoint.getArgs();
        Object data = args[0];

        try{
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(data);

            // 获取签名，请求头的
            ServletRequestAttributes requestAttributes =
                    (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

            HttpServletRequest request = requestAttributes.getRequest();

            String signature = request.getHeader("signature");

            // 验证签名
            boolean verify = RSAUtils.verify(json, signature);

            if (!verify) {
                throw new RuntimeException("验证签名失败！");
            }
        }catch (Throwable e) {

            throw new RuntimeException("签名异常：" + e.getMessage());
        }
    }
}
