package com.example.server.utils;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAUtils {

    private static String priKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDgw5lEwewPtIcM\n" +
            "l+JlNTXb44wfVrmCoszc7hUJ4uXJBVID48JlVnUF1AYGNwAuZeNoRz5dRacmvEUA\n" +
            "3QknKwo6RRFs/ZI3DceQpa+3wW8+eq0Nf+KdVueskLormrP/UHZVcGlU1rVzrLF8\n" +
            "iCnJd4ktGqfduoblznV7FFVFa2I6YQhwi+nrPcPvjUDtgzkVx5r/6cAiigdqUhpE\n" +
            "kGzPL33h7/nOfHQQ7Ys3ShqLsYi2KhkzIGfHkzXVOkBAUdh6dTxSwAjPeUhWDEF9\n" +
            "A4r/J4XPxvW4t9irvd2vqWYrTKs3540C+r7DSiE8V2NUWCx60N84Ojh1qEZloMZd\n" +
            "eUw6ASKZAgMBAAECggEAUIiFEEfTh0UN5os9SToplCISAOCYE6C1equryiEMaFyJ\n" +
            "qG8hJiI2R+JSTkPCwt0rACAWzmC4pX5r0wVf1DLttTcuKTkp/KPpT4CuV07txxs5\n" +
            "ik66KdoLpJ8utZT2zZJM9AwVEZrk5Q+JgK9hGZjFA+47G41L7GdvXKxKaVcymOuj\n" +
            "ZxKM56hdqmBtmJ70DjREhvCB7ABiuXB6yVoEIgJVrV1jiHZUOhPHWkJoQId9Gmp9\n" +
            "+LJVoyXxCrX5kvouAW5GF2P0HbIAAqHK3IkONeqISDHhE8EHBdLFsU6lks0TLzsQ\n" +
            "Rd94lzqJA1iItYWHuo4esygmeAw6/0QHCtMkCkJ2KQKBgQD9+H5LqdZMQp4QrsEZ\n" +
            "v6Hoc6qb7KXqb3UEDrEwaB+dOXPN775bUDan+8B1N7v6EG0MnSHX7Rs3c+bu/DWa\n" +
            "zsQBRQzyDedrvAwi8NXYhe6YAsWLhDbuC894TIkSoU5xlux6d88fJI1mF9HQuKm8\n" +
            "uK7/0AAwev72ih3sVpSz7DKzawKBgQDij1yziKrZWVvcDHjw5z+5L1QsQChJRACw\n" +
            "q928sliNsov9waLE0zQ+ol4enPCXBWsmPHbOlsWoP12bFXPcNhnDklUY9k/uCa6v\n" +
            "6SxWUcrynI4wYpL7xnBwox370+xTAVn39FVlEaG2m9Z8iry0WkKQnZfsSTaconX7\n" +
            "5qSXSSKHCwKBgQDXtegPfAF4ZFy87rE0nX93YEbYiPejV2ecOp5k7wtrDxE9KRQV\n" +
            "mtvRchLVfLE8hBcbT5sfO1SSlO7grXcaNeRk77E/JnAZIW9tfgEkFx3AHxqKlZHS\n" +
            "PlXJZ7hFCp0F/Zp9qqVRXkp7HGApV69Ti8mXnaOLiexkmosv4ZoL9+JYYQKBgQC8\n" +
            "LVtP+3n8oY38zZc+cLSq+bauMnPDtunj1DyOWIvY1a4VDNpHJS3FwFLwlmX26hc/\n" +
            "YQVFPlHaFxeGkmDzTTRFDqwD2cI18nDXx5J9993beVFYuudenEoUJEfpPPKTVxIF\n" +
            "MzoigmyRIgsa0pk+K6ISP/NJ3N2xZ/cbTUz0I1i8VQKBgBxuXo9JiNFyAEeUEdSo\n" +
            "XIQ2WwdoVSQZvTdsWL65nQj++9pJuzRljDTFYK1/Q9zbK+aac2O9QPZKS/943/Of\n" +
            "y9f/6jbI99nFRkLZrhvwwmvVunFIEggrGrC0RwqscpY9o1X8/TSjHakGVII4zq/n\n" +
            "TM9X+fp3SrXMVC/k0jnyiIcY";

    private static String pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4MOZRMHsD7SHDJfiZTU1\n" +
            "2+OMH1a5gqLM3O4VCeLlyQVSA+PCZVZ1BdQGBjcALmXjaEc+XUWnJrxFAN0JJysK\n" +
            "OkURbP2SNw3HkKWvt8FvPnqtDX/inVbnrJC6K5qz/1B2VXBpVNa1c6yxfIgpyXeJ\n" +
            "LRqn3bqG5c51exRVRWtiOmEIcIvp6z3D741A7YM5Fcea/+nAIooHalIaRJBszy99\n" +
            "4e/5znx0EO2LN0oai7GItioZMyBnx5M11TpAQFHYenU8UsAIz3lIVgxBfQOK/yeF\n" +
            "z8b1uLfYq73dr6lmK0yrN+eNAvq+w0ohPFdjVFgsetDfODo4dahGZaDGXXlMOgEi\n" +
            "mQIDAQAB";

    public static String sign(String originText) throws Throwable{

        // 将私钥转换为java 对象
        PrivateKey privateKey = KeyFactory.getInstance("RSA")
                .generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(priKey)));

        // 构造签名工具对象
        Signature signature = Signature.getInstance("NONEwithRSA");

        // 使用RSA算法进行签名，需要配置签名时使用的私钥对象
        signature.initSign(privateKey);

        // 输入数据原文
        signature.update(originText.getBytes());

        // 开始签名
        byte[] signBytes = signature.sign();

        return Base64.encodeBase64String(signBytes);
    }

    public static boolean verify(String originText, String sign) throws Throwable{

        PublicKey publicKey = KeyFactory.getInstance("RSA")
                .generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(pubKey)));

        Signature signature = Signature.getInstance("NONEwithRSA");

        // 注册验证签名需要的publicKey
        signature.initVerify(publicKey);

        // 注册原文
        signature.update(originText.getBytes());

        return signature.verify(Base64.decodeBase64(sign));
    }

    public static void main(String[] args) throws Throwable {

        // 生成签名
        String sign = sign("{\"firstName\":\"virgil\",\"lastName\":\"Stark\"}");
        System.out.println(sign);

        boolean result = verify("{\"firstName\":\"virgil\",\"lastName\":\"Stark\"}", sign);
        System.out.println(result);

        // RSA 做签名
        // HS256 做签名
        // 为了防止数据被伪造：
        /**
         * 将签名设计到接口的调用中；
         * 我们写一个服务器，暴露接口 /test/1
         * 任何人在调用我的接口的时候，必须提供一个签名，由服务器验证签名是否正确
         *
         *
         */
    }
}
