package com.example.server.mapper;

import com.example.server.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {

    List<Admin> findAllWithRoles2();

    List<Admin> findAllWithRoles();

    /**
     * 使用JavaBean对象进行传值
     * 该对象种的属性，就可以作为xml文件种的#{}内的变量来进行赋值
     * @param admin
     * @return
     */
    List<Admin> findByConditions(Admin admin);

    /**
     * 使用@Param注解来进行传值
     */
    @Select("select * from t_admin where id = #{abc}")
    Admin findById(@Param("abc") Integer id);

    /**
     * 使用@Param注解可以区分多个参数，如果修饰的参数是一个JavaBean，那么
     * -@Param注解种的value值需要作为前缀
     */
    @Select("select * from t_admin where name = #{name1} and phone = #{admin.phone}")
    List<Admin> findByNameAndPhone(@Param("name1") String name,
                                   @Param("admin") Admin admin);

    @Select("select * from t_admin where name = #{name} and phone = #{phone}")
    List<Admin> findByNameAndPhoneMap(Map<String, String> params);

    @Select("select count(1) from t_admin")
    Integer findAllCount();

    int insertBatchAdmin(@Param("admins") Admin[] admins);
}
