package com.example.server.mapper;

import com.example.server.pojo.SysYebLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-11-12
 */
public interface SysYebLogMapper extends BaseMapper<SysYebLog> {

}
