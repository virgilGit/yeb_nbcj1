package com.example.server.mapper;

import com.example.server.pojo.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> findAllWithEmps();
}
