package com.example.server.conf;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 标记当前类是一个spring中的配置类
 */
@Configuration
public class MyBatisPlusConfig {

    /**
     * @Bean修饰方法，表示向spring容器中注册一个对象，交给spring统一管理；
     * @Bean的功能其实和@Controller @Service 类似
     * @Bean是修饰方法的，一般他可以用来注册一些第三方的组件对象；
     *
     * @Controoler
     * public class TestCOntroller {
     *
     * }
     *
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
