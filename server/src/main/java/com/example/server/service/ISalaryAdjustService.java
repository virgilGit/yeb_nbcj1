package com.example.server.service;

import com.example.server.pojo.SalaryAdjust;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface ISalaryAdjustService extends IService<SalaryAdjust> {

}
