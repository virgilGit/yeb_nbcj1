package com.example.server.service.impl;

import com.example.server.pojo.Admin;
import com.example.server.mapper.AdminMapper;
import com.example.server.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 *  final 能不能修饰抽象类？
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public List<Admin> getByConditions(Admin admin) {

        return adminMapper.findByConditions(admin);
    }

    @Override
    public Integer getAllCount() {
        return adminMapper.findAllCount();
    }

    @Override
    public boolean saveBatchAdmin(List<Admin> admins) {

        int result = adminMapper.insertBatchAdmin(admins.toArray(new Admin[admins.size()]));

        return result == admins.size();
    }
}
