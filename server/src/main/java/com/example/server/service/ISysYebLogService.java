package com.example.server.service;

import com.example.server.pojo.SysYebLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小红
 * @since 2022-11-12
 */
public interface ISysYebLogService extends IService<SysYebLog> {

}
