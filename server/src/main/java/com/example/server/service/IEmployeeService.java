package com.example.server.service;

import com.example.server.pojo.Employee;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.vo.PageBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface IEmployeeService extends IService<Employee> {

    PageBean<Employee> getAllPage(Integer currentPage, Integer size);

    PageBean<Employee> getAllPage2(Integer currentPage, Integer size);
}
