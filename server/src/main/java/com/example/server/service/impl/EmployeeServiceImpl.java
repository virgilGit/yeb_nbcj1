package com.example.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.server.pojo.Employee;
import com.example.server.mapper.EmployeeMapper;
import com.example.server.service.IEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.server.vo.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public PageBean<Employee> getAllPage(Integer currentPage, Integer size) {

        PageBean<Employee> pageBean = new PageBean<>();

        // select * from t_employee limit size * (currentPage - 1), size;
        List<Employee> rows = employeeMapper.findAllInPage(
                size * (currentPage - 1),
                size
        );

        // 如果不分页，总共应该有多少条数据
        Integer total = employeeMapper.selectCount(new QueryWrapper<Employee>());

        // 计算总页码 totalPage = ceil (total / size); 11 / 5 = 2.X -> 3 向上取整
        // 11.0 / 5.0 = 2.x
        // total * 1.0 int -> double 自定转型，将精度低的转换为精度高的
        // (int) Math.ceil(xxxx) double -> int 将精度高的 -> 精度低的，就需要手动强转，这个过程会损失数据的精度
        int totalPage = (int) Math.ceil(total * 1.0 / size);

        return pageBean.setCurrentPage(currentPage)
                .setRows(rows)
                .setTotal(total)
                .setTotalPage(totalPage);
    }

    @Override
    public PageBean<Employee> getAllPage2(Integer currentPage, Integer size) {

        Page<Employee> page = new Page<>(currentPage, size);

        // 通过自定义mapper实现分页
        // mapper方法的第一个参数必须是page对象
        IPage<Employee> result = employeeMapper.findAllInPage2(page);

        return new PageBean<Employee>()
                .setCurrentPage(currentPage)
                .setRows(result.getRecords())
                .setTotal((int) result.getTotal())
                .setTotalPage((int) result.getPages());
    }
}
