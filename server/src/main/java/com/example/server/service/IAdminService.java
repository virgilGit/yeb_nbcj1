package com.example.server.service;

import com.example.server.pojo.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface IAdminService extends IService<Admin> {

    List<Admin> getByConditions(Admin admin);

    Integer getAllCount();

    boolean saveBatchAdmin(List<Admin> admins);
}
