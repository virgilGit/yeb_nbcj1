package com.example.server.service.impl;

import com.example.server.pojo.SysYebLog;
import com.example.server.mapper.SysYebLogMapper;
import com.example.server.service.ISysYebLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小红
 * @since 2022-11-12
 */
@Service
public class SysYebLogServiceImpl extends ServiceImpl<SysYebLogMapper, SysYebLog> implements ISysYebLogService {

}
