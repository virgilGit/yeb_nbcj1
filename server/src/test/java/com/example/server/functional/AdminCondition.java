package com.example.server.functional;

import com.example.server.pojo.Admin;

@FunctionalInterface // 函数式接口：有且只能有一个抽象方法
public interface AdminCondition<T> {

    boolean testAdmin(T admin);

//    boolean test2();

    // 类似于类的静态方法
    default String test() {
        return null;
    }
}
