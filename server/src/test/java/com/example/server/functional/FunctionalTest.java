package com.example.server.functional;

import com.example.server.pojo.Admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FunctionalTest {

    /**
     * 对adminlist进行筛选
     * 按照admin#id > 10
     * 筛选所有enable 为true的值
     * @return
     */
    public static List<Admin> filterAdmin(List<Admin> resourceList, AdminCondition<Admin> adminCondition) {

        List<Admin> targetList = new ArrayList<>();

        for (Admin admin : resourceList) {

            // 进行筛选
            if (adminCondition.testAdmin(admin)) {
                targetList.add(admin);
            }
        }

        return targetList;
    }

    public static void main(String[] args) {

        List<Admin> admins = Arrays.asList(
                new Admin().setId(11).setUsername("admin11"),
                new Admin().setId(8).setUsername("admin8"),
                new Admin().setId(13).setUsername("admin13"),
                new Admin().setId(14).setUsername("admin14"),
                new Admin().setId(9).setUsername("admin9"),
                new Admin().setId(1).setUsername("admin1")
        );
        // 1. 这个方法就是个普通方法，只能new一个对象，再调用方法
//        new FunctionalTest().filterAdmin()
        // 2. 将方法改成静态方法，这个方法属于类对象，不属于实例对象
        List<Admin> result = filterAdmin(admins, admin -> admin.getId() <= 10);

        for (Admin admin : result) {
            System.out.println(admin);
        }
        result.forEach(x -> System.out.println(x));
    }
}
