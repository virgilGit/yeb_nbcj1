package com.example.server.proxy;

public interface IActor {
    
    /**
     * 基本演出
     */
    void basicAct(float money);

    /**
     * 危险演出
     */
    void dangerAct(float money);
}