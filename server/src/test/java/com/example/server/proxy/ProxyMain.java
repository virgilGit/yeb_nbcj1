package com.example.server.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyMain {

    public static void main(String[] args) {

        Actor actor = new Actor();

        IActor proxyInstance = (IActor) Proxy.newProxyInstance(
                actor.getClass().getClassLoader(),
                actor.getClass().getInterfaces(),
                // 匿名内部类
                // 构造一个接口对象，接口是不能直接new一个对象的
                // java中支持匿名内部类，可以在构造对象的时候，先构造一个实现类，再构造对象
                // TODO lambda 表达式 java8的技术
                // 如果一个接口是一个函数式接口，就可以使用lambda表达式来新建一个匿名内部类对象
                // 函数式接口表示这个接口有且仅有一个 抽象方法，就可以使用lambda表达式来手动编写接口中唯一的方法的实现
                // 需要提供参数列表
                // list.forEach(System.out::println) -> 方法引用，是Lambda的缩写
                // default
                (proxy, method, args1) -> {

                    String methodName = method.getName();

                    if ("basicAct".equals(methodName)) {

                        float money = (float) args1[0];
                        if (money > 1000) {
                            method.invoke(actor, args1);
                        } else {
                            System.out.println("金额小于1000，演员被中介拒绝出演！");
                        }
                    } else if ("dangerAct".equals(methodName)) {

                        float money = (float) args1[0];
                        if (money > 2000) {
                            method.invoke(actor, args1);
                        } else {
                            System.out.println("金额小于2000，演员被中介拒绝出演！");
                        }
                    } else {
                        System.out.println("没有这个方法！");
                    }
                    return null;
                }
        );

        actor.basicAct(1000);
        actor.dangerAct(1500);

        proxyInstance.basicAct(1100);
        proxyInstance.dangerAct(1500);
    }
}
