package com.example.server.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.server.pojo.Admin;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdminMapperTest {

    @Autowired
    private AdminMapper adminMapper;

    @Test
    public void test1() {

        List<Admin> admins = adminMapper.selectList(new QueryWrapper<Admin>());

        admins.forEach(System.out::println);
    }

    @Test
    public void testFindByNameAndPhoneMap() {

        Map<String, String> params = new HashMap<>();
        params.put("name", "系统管理员");
        params.put("phone", "13812361398");
        List<Admin> list = adminMapper.findByNameAndPhoneMap(params);

        list.forEach(System.out::println);
    }

    @Test
    public void testInsertBatchAdmin() {

        Admin[] admins = new Admin[5];
        admins[0] = new Admin().setName("xiaoA").setUsername("zzz");
        admins[1] = new Admin().setName("xiaoB").setUsername("zzz");
        admins[2] = new Admin().setName("xiaoC").setUsername("zzz");
        admins[3] = new Admin().setName("xiaoD").setUsername("zzz");
        admins[4] = new Admin().setName("xiaoE").setUsername("zzz");
        int i = adminMapper.insertBatchAdmin(admins);
        System.out.println(i);
    }

    @Test
    public void testFindAllWithRoles() {

        List<Admin> admins = adminMapper.findAllWithRoles();

        admins.forEach(System.out::println);
    }

    @Test
    public void testFindAllWithRoles2() {
        List<Admin> admins = adminMapper.findAllWithRoles2();
        admins.forEach(System.out::println);
    }
}