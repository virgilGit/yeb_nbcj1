package com.example.server.mapper;

import com.example.server.pojo.Employee;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EmployeeMapperTest extends TestCase {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void testFindAllWithDept() {

        List<Employee> allWithDept = employeeMapper.findAllWithDept();

        allWithDept.forEach(System.out::println);
    }
}