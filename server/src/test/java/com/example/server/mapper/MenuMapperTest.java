package com.example.server.mapper;

import com.example.server.pojo.Menu;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MenuMapperTest extends TestCase {

    @Autowired
    private MenuMapper menuMapper;

    @Test
    public void testFindByParentId() throws JsonProcessingException {

        List<Menu> menus = menuMapper.findByParentId(null);
        menus.forEach(System.out::println);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(menus);
        System.out.println(json);
    }
}