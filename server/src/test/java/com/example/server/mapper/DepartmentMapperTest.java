package com.example.server.mapper;

import com.example.server.pojo.Department;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DepartmentMapperTest extends TestCase {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Test
    public void testFindAllWithEmps() {

        List<Department> allWithEmps = departmentMapper.findAllWithEmps();

        allWithEmps.forEach(System.out::println);
    }
}